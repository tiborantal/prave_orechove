
//NAVIGATION  

function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}


//BACK TO TOP
var backToTop = $(".back-to-top");

    backToTop
        .hide()
        .appendTo("body")
        .on("click", function() {
         $("body").animate({ scrollTop: 0 }, 1000);
    });


var win = $(window);
    win.on("scroll", function() {
        if (win.scrollTop() > 500 ) backToTop.fadeIn();
        else backToTop.hide();
    });
    


// MAPS

var markers = [];

    function initialize() {
    
        var shops = [
            ['Zdravá strava, ul. M. R. Štefánika 29', 48.772128, 18.604730, 1],
            ['Activ-shop, Hornopotočná 1', 48.378834, 17.587013, 1],
            ['Harmony, OC Tesco, Zvolenká cesta 8', 48.714138, 19.136656, 2],
            ['Fitness.Market, Lúčky 1532/12', 48.814295, 17.154813, 2],
        ];
    
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: new google.maps.LatLng(48.66, 19.56),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
        });

        map.addListener('click', function() {
          map.set('scrollwheel', true);
});
    
        var infowindow = new google.maps.InfoWindow();
    
        for (var i = 0; i < shops.length; i++) {
    
            var newMarker = new google.maps.Marker({
                position: new google.maps.LatLng(shops[i][1], shops[i][2]),
                map: map,
                title: shops[i][0]
            });
    
            google.maps.event.addListener(newMarker, 'click', (function (newMarker, i) {
                return function () {
                    infowindow.setContent(shops[i][0]);
                    infowindow.open(map, newMarker);
                }
            })(newMarker, i));
    
            markers.push(newMarker);
        }
    }
    
    initialize();




//HOVER ESHOP CSS ANIMATE

$(document).ready(function() {
    
    /* Every time the window is scrolled ... */
    $(window).scroll( function(){
    
        /* Check the location of each desired element */
        $('.hideme').each( function(i){
            
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            if( bottom_of_window > bottom_of_object ){
                
                $(this).css({'opacity':'1'}, 6000 );
                    
            }
            
        }); 
    
    });
    
});


//SCROLLOVANIE ANIMACIA

var s_butter = $(".butter_images"),
    s_links = s_butter.find("a");

 s_links.on("click", function(event) {
 $("html,body").animate( { scrollTop: $(this.hash).offset().top }, 1000);
 event.preventDefault();
});

    
$(document).ready(function() {

  //window and animation items
  var animation_elements = $.find('.animation-element');
  var web_window = $(window);

  //check to see if any animation containers are currently in view
  function check_if_in_view() {
    //get current window information
    var window_height = web_window.height();
    var window_top_position = web_window.scrollTop();
    var window_bottom_position = (window_top_position + window_height);

    //iterate through elements to see if its in view
    $.each(animation_elements, function() {

      //get the element sinformation
      var element = $(this);
      var element_height = $(element).outerHeight();
      var element_top_position = $(element).offset().top;
      var element_bottom_position = (element_top_position + element_height);

      //check to see if this current container is visible (its viewable if it exists between the viewable space of the viewport)
      if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
        element.addClass('in-view');
      } else {
        element.removeClass('in-view');
      }
    });

  }

  //on or scroll, detect elements in view
  $(window).on('scroll resize', function() {
      check_if_in_view()
    })
    //trigger our scroll event on initial load
  $(window).trigger('scroll');

});


//CONTACT FORM

function _(id){ return document.getElementById(id); }
function submitForm(){
  _("submit-btn").disabled = true;
  _("status").innerHTML = 'please wait ...';
  var formdata = new FormData();
  formdata.append( "n", _("n").value );
  formdata.append( "e", _("e").value );
  formdata.append( "t", _("t").value );
  formdata.append( "m", _("m").value );
  var ajax = new XMLHttpRequest();
  ajax.open( "POST", "example_parser.php" );
  ajax.onreadystatechange = function() {
    if(ajax.readyState == 4 && ajax.status == 200) {
      if(ajax.responseText == "success"){
        _("my_form").innerHTML = '<h2>Ďakujem za správu, čoskoro sa ozvem.</h2>';
      } else {
        _("status").innerHTML = ajax.responseText;
        _("submit-btn").disabled = false;
      }
    }
  }
  ajax.send( formdata );
}
