var peanut_p    = $(".peanut_s a.control_next");
var peanut_n    = $(".peanut_s a.control_prev");

var walnut_p    = $(".walnut_s a.control_next");
var walnut_n    = $(".walnut_s a.control_prev");

var almond_p    = $(".almond_s a.control_next");
var almond_n    = $(".almond_s a.control_prev");

var hazelnut_p  = $(".hazelnut_s a.control_next");
var hazelnut_n  = $(".hazelnut_s a.control_prev");



//PEANUT

jQuery(document).ready(function ($) {

  $('#checkbox').change(function(){
    setInterval(function () {
        moveRight();
    }, 3000);
  });
  
	var slideCount = $('.peanut_s ul li').length;
	var slideWidth = $('.peanut_s ul li').width();
	var slideHeight = $('.peanut_s ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('.peanut_s').css({ width: slideWidth, height: slideHeight });
	
	$('.peanut_s ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('.peanut_s ul li:last-child').prependTo('.peanut_s ul');

    function moveLeft() {
        $('.peanut_s ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('.peanut_s ul li:last-child').prependTo('.peanut_s ul');
            $('.peanut_s ul').css('left', '');
        });
    };

    function moveRight() {
        $('.peanut_s ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('.peanut_s ul li:first-child').appendTo('.peanut_s ul');
            $('.peanut_s ul').css('left', '');
        });
    };

   peanut_p.click(function () {
        moveLeft();
    });

   peanut_n.click(function () {
        moveRight();
    });
    return false;
});  



//WALNUT

jQuery(document).ready(function ($) {

  $('#checkbox').change(function(){
    setInterval(function () {
        moveRight();
    }, 3000);
  });
  
    var slideCount = $('.walnut_s ul li').length;
    var slideWidth = $('.walnut_s ul li').width();
    var slideHeight = $('.walnut_s ul li').height();
    var sliderUlWidth = slideCount * slideWidth;
    
    $('.walnut_s').css({ width: slideWidth, height: slideHeight });
    
    $('.walnut_s ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
    
    $('.walnut_s ul li:last-child').prependTo('.walnut_s ul');

    function moveLeft() {
        $('.walnut_s ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('.walnut_s ul li:last-child').prependTo('.walnut_s ul');
            $('.walnut_s ul').css('left', '');
        });
    };

    function moveRight() {
        $('.walnut_s ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('.walnut_s ul li:first-child').appendTo('.walnut_s ul');
            $('.walnut_s ul').css('left', '');
        });
    };

    walnut_p.click(function () {
        moveLeft();
    });

    walnut_n.click(function () {
        moveRight();
    });
    return false;
});    



//ALMOND

jQuery(document).ready(function ($) {

  $('#checkbox').change(function(){
    setInterval(function () {
        moveRight();
    }, 3000);
  });
  
    var slideCount = $('.almond_s ul li').length;
    var slideWidth = $('.almond_s ul li').width();
    var slideHeight = $('.almond_s ul li').height();
    var sliderUlWidth = slideCount * slideWidth;
    
    $('.almond_s').css({ width: slideWidth, height: slideHeight });
    
    $('.almond_s ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
    
    $('.almond_s ul li:last-child').prependTo('.almond_s ul');

    function moveLeft() {
        $('.almond_s ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('.almond_s ul li:last-child').prependTo('.almond_s ul');
            $('.almond_s ul').css('left', '');
        });
    };

    function moveRight() {
        $('.almond_s ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('.almond_s ul li:first-child').appendTo('.almond_s ul');
            $('.almond_s ul').css('left', '');
        });
    };

    almond_p.click(function () {
        moveLeft();
    });

    almond_n.click(function () {
        moveRight();
    });
    return false;
});    



//HAZELNUT

jQuery(document).ready(function ($) {

  $('#checkbox').change(function(){
    setInterval(function () {
        moveRight();
    }, 3000);
  });
  
    var slideCount = $('.hazelnut_s ul li').length;
    var slideWidth = $('.hazelnut_s ul li').width();
    var slideHeight = $('.hazelnut_s ul li').height();
    var sliderUlWidth = slideCount * slideWidth;
    
    $('.hazelnut_s').css({ width: slideWidth, height: slideHeight });
    
    $('.hazelnut_s ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
    
    $('.hazelnut_s ul li:last-child').prependTo('.hazelnut_s ul');

    function moveLeft() {
        $('.hazelnut_s ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('.hazelnut_s ul li:last-child').prependTo('.hazelnut_s ul');
            $('.hazelnut_s ul').css('left', '');
        });
    };

    function moveRight() {
        $('.hazelnut_s ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('.hazelnut_s ul li:first-child').appendTo('.hazelnut_s ul');
            $('.hazelnut_s ul').css('left', '');
        });
    };

    hazelnut_p.click(function () {
        moveLeft();
    });

    hazelnut_n.click(function () {
        moveRight();
    });
    return false;
});    